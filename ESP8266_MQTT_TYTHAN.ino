/*
 Basic ESP8266 MQTT example
 This sketch demonstrates the capabilities of the pubsub library in combination
 with the ESP8266 board/library.
 It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" every two seconds
  - subscribes to the topic "inTopic", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "inTopic" is an 1, switch ON the ESP Led,
    else switch it off
 It will reconnect to the server if the connection is lost using a blocking
 reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
 achieve the same result without blocking the main loop.
 To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"
*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Servo.h>

// Motor A : D1 D3
// Motor B : D2 D4

// Update these with values suitable for your network.

const char* ssid = "MI SSID";
const char* password = "MI CONTRASEÑA";
const char* mqtt_server = "ioticos.org";

const char* mqtt_usuario = "fU6qSDlMYEKtCo0";
const char* mqtt_pssw = "YausPUJPBBRp3pU";
const char* topico_a_suscribir = "nEOt5H7qt54ezHC";

WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE	(50)
char msg[MSG_BUFFER_SIZE];
int value = 0;


// Servos
Servo servo_cabeza;
Servo servo_direccion;
Servo servo_brazo;
int direccion = 90;

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);

  String payloadStr = "";
  
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    payloadStr += (char)payload[i];
  }
  Serial.println();

  if((String)topic == (String)topico_a_suscribir){

      if(payloadStr == "o" or payloadStr == "O"){
        servo_brazo.write(170);
      }

      if(payloadStr == "l" or payloadStr == "L"){
        servo_brazo.write(10);
      }

      if(payloadStr == "a" or payloadStr == "A"){

        if (direccion >= 135) {
          direccion = 135;
        }else{
          direccion += 15;
        }
        servo_direccion.write(direccion);
      }

      if(payloadStr == "d" or payloadStr == "D"){

        if (direccion <= 45) {
          direccion = 45;
        }else{
          direccion -= 15;
        }
        
        servo_direccion.write(direccion);
      }

      // Detención
      if(payloadStr == "z" or payloadStr == "Z"){
        digitalWrite(D1, LOW);
        digitalWrite(D3, LOW);
      }

      // Adelante
      if(payloadStr == "w" or payloadStr == "W"){
        digitalWrite(D1, HIGH);
        digitalWrite(D3, LOW);
      }

      // Atrás
      if(payloadStr == "s" or payloadStr == "S"){
        digitalWrite(D1, HIGH);
        digitalWrite(D3, HIGH);
      }
  }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str(), mqtt_usuario, mqtt_pssw)) {
      Serial.println("connected");
      client.publish(topico_a_suscribir, "TYTHAN 01 Conectado");
      // ... and resubscribe
      // client.subscribe("vePBC4yrxskw6rv/entrada");
      client.subscribe(topico_a_suscribir);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);


  // Servos
  servo_direccion.attach(D8);
  servo_brazo.attach(D7);
  servo_cabeza.attach(D6);

  servo_direccion.write(direccion);
  delay(500);
  servo_brazo.write(90);
  delay(500);
  servo_cabeza.write(90);
  delay(500);

  // Comandos motos
  pinMode(D1, OUTPUT);
  pinMode(D3, OUTPUT);
  
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

}
